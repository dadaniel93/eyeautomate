package custom.selenium;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import eyeautomate.ScriptRunner;

public class FullscreenBrowser
{
	private static final String COMMAND="FullscreenBrowser";
	private static final String ICON_FILENAME="icons/monitor2.png";
	private WebDriver webDriver=null;

	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		if(webDriver==null)
		{
			scriptParameters.put("Error", "No browser open");
			return false;
		}

		try
		{
			webDriver.manage().window().fullscreen();
			return true;
		}
		catch(Exception e)
		{
			scriptParameters.put("Error", "Exception: "+e.toString());
			return false;
		}
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/seleniumcommands.html";
	}

	public String getTooltip()
	{
		return "<html>Expand browser to fullscreen<br/>Usage:<br/>"+COMMAND+"</html>";
	}

	public void setScriptRunner(ScriptRunner scriptRunner)
	{
		webDriver=(WebDriver)scriptRunner.getWebDriver();
	}
}
