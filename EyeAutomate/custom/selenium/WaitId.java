package custom.selenium;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import eyeautomate.ScriptRunner;

public class WaitId
{
	private static final String COMMAND="WaitId";
	private static final String ICON_FILENAME="icons/hourglass2.png";
	private WebDriver webDriver=null;
	ScriptRunner scriptRunner=null;

	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		if(commandParameters.length<1)
		{
			scriptParameters.put("Error", "Missing parameter in command. Usage: WaitId Id");
			return false;
		}

		String id=commandParameters[0].trim();

		if(webDriver==null)
		{
			scriptParameters.put("Error", "No browser open");
			return false;
		}

		try
		{
			WebDriverWait wait = new WebDriverWait(webDriver, scriptRunner.getVizionEngine().getTimeout());
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id(id)));
			return true;
		}
		catch(Exception e)
		{
			scriptParameters.put("Error", "Exception: "+e.toString());
			return false;
		}
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<String: Enter Id>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/seleniumcommands.html";
	}

	public String getTooltip()
	{
		return "<html>Waits for a widget identified by an id<br/>Usage:<br/>"+COMMAND+" Id</html>";
	}

	public void setScriptRunner(ScriptRunner scriptRunner)
	{
		this.scriptRunner=scriptRunner;
		webDriver=(WebDriver)scriptRunner.getWebDriver();
	}
}
