package custom.basic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import eyeautomate.ScriptRunner;

public class Call
{
	private static final String COMMAND="Call";
	private static final String ICON_FILENAME="icons/call2.png";
	private ScriptRunner scriptRunner=null;
	private Properties scriptParameters;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		this.scriptParameters=scriptParameters;
		List<String> commandLine=new ArrayList<String>(Arrays.asList(commandParameters));
		commandLine.add(0, COMMAND);
		boolean passed=performCommand("", COMMAND, 1, commandLine);
		if(!passed)
		{
			scriptRunner.setScreenshot(scriptRunner.getLatestScreenCapture());
		}
		return passed;
	}

	public String getTooltip()
	{
		return "<html>Calls another script<br/>Usage: <br/>"+COMMAND+" Script<br/>"+COMMAND+" [Iterations] [Parameters] Script</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<Script>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/basiccommands.html";
	}
	
	public void setScriptRunner(ScriptRunner scriptRunner)
	{
		this.scriptRunner=scriptRunner;
	}

	private void errorLogMessage(String scriptFilename, String text, int lineNo)
	{
		scriptParameters.put("Error", text);
	}

	private boolean performCommand(String scriptFilename, String command, int lineNo, List<String> commandLine)
	{
		if (commandLine.size() < 2)
		{
			errorLogMessage(scriptFilename, "Missing parameter in Call command. Usage: Call [Iterations] [Parameters] Script", lineNo);
			return false;
		}
		else
		{
			String[] params = new String[commandLine.size() - 1];
			for (int i = 1; i < commandLine.size(); i++)
			{
				params[i - 1] = commandLine.get(i);
			}

			ScriptRunner sr=scriptRunner.createChildScriptRunner();
			if (sr.runScriptInternal(params) == false)
			{
				// Script failed
				scriptRunner.removeChildScriptRunner(sr);
				return false;
			}
			scriptRunner.removeChildScriptRunner(sr);
		}

		return true;
	}
}
