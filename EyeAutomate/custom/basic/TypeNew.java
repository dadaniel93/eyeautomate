package custom.basic;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class TypeNew
{
    private static final String COMMAND="Type";
    private static final String ICON_FILENAME="icons/write2.png";
    private Properties scriptParameters;
    public Robot robot = null;
    String OS = System.getProperty("os.name").toLowerCase();
    int writeDelay = 50;

    public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
    {
        this.scriptParameters=scriptParameters;
        List<String> commandLine=new ArrayList<String>(Arrays.asList(commandParameters));
        commandLine.add(0, COMMAND);
        return performCommand("", COMMAND, 1, commandLine);
    }

    public String getTooltip()
    {
        return "<html>Simulate text typed by the keyboard<br/>Usage: <br/>"+COMMAND+" Text</html>";
    }

    public String[] getParameters()
    {
        return new String[]{"Error"};
    }

    public String getCommand()
    {
        return COMMAND+" \"<String: Enter text>\"";
    }

    public String getIconFilename()
    {
        return ICON_FILENAME;
    }

    public String getHelp()
    {
        return "http://www.eyeautomate.com/basiccommands.html";
    }

    private void errorLogMessage(String scriptFilename, String text, int lineNo)
    {
        scriptParameters.put("Error", text);
    }

    private boolean performCommand(String scriptFilename, String command, int lineNo, List<String> commandLine)
    {
        if (commandLine.size() < 2)
        {
            errorLogMessage(scriptFilename, "Missing parameter in Type command. Usage: "+COMMAND+" Text", lineNo);
            return false;
        }
        else
        {
            // Concatenate all parameters
            StringBuffer textBuffer=new StringBuffer();
            for (int i = 1; i < commandLine.size(); i++)
            {
                if(i>1)
                {
                    textBuffer.append(" ");
                }
                String textParam = commandLine.get(i);
                //textBuffer.append(scriptRunner.decrypt(textParam));
                textBuffer.append(textParam);
            }
            String text = textBuffer.toString();


            //eye.type(text);
            typeRobot(text);
        }

        return true;
    }

    public void typeRobot(String text) {
        boolean isWindows = this.isWindows();
        int length = text.length();

        for(int i = 0; i < length; ++i) {
            this.writeDelayIfNeeded();
            char character = text.charAt(i);
            this.type(character, false, isWindows);

        }

    }

    private void type(char character, boolean special, boolean isWindows) {
        try {
            switch(character) {
            case '\t':
                this.doType(9);
                break;
            case '\n':
                this.doType(10);
                break;
            case '\u000b':
            case '\f':
            case '\r':
            case '\u000e':
            case '\u000f':
            case '\u0010':
            case '\u0011':
            case '\u0012':
            case '\u0013':
            case '\u0014':
            case '\u0015':
            case '\u0016':
            case '\u0017':
            case '\u0018':
            case '\u0019':
            case '\u001a':
            case '\u001b':
            case '\u001c':
            case '\u001d':
            case '\u001e':
            case '\u001f':
            case '!':
                this.doType(KeyEvent.VK_EXCLAMATION_MARK);
                break;
            case '"':
            case '#':
            case '$':
                this.doType(KeyEvent.VK_DOLLAR);
                break;
            case '%':
            case '&':
                this.doType(KeyEvent.VK_SHIFT,KeyEvent.VK_7);
                break;
            case '\'':
                this.doType(KeyEvent.VK_BACK_SLASH);
                break;
            case '(':
                this.doType(KeyEvent.VK_LEFT_PARENTHESIS);
                break;
            case ')':
                this.doType(KeyEvent.VK_RIGHT_PARENTHESIS);
                break;
            case '*':
            case '+':
                this.doType(KeyEvent.VK_PLUS);
                break;
            case ',':
                this.doType(KeyEvent.VK_COMMA);
                break;
            case '-':
                this.doType(KeyEvent.VK_MINUS);
                break;
            case '.':
                this.doType(KeyEvent.VK_PERIOD);
                break;
            case '/':
                this.doType(KeyEvent.VK_SLASH);
                break;
            case ':':
                this.doType(KeyEvent.VK_SHIFT,KeyEvent.VK_SEMICOLON);
                break;
            case ';':
            case '<':
            case '=':
                this.doType(KeyEvent.VK_EQUALS);
                break;
            case '>':
            case '?':
                this.doType(KeyEvent.VK_SHIFT,KeyEvent.VK_SLASH);
                break;
            case '@':
            case '[':
                this.doType(KeyEvent.VK_OPEN_BRACKET);
                break;
            case '\\':
            case ']':
                this.doType(KeyEvent.VK_CLOSE_BRACKET);
                break;
            case '^':
            case '_':
                this.doType(KeyEvent.VK_SHIFT,KeyEvent.VK_MINUS);
                break;
            case '`':
            default:
                if (isWindows) {
                    //this.typeUnicode(character);
                } else {
                    switch(character) {
                        case ',':
                            this.doType(44);
                            return;
                        case '-':
                        default:
                            return;
                        case '.':
                            this.doType(46);
                    }
                }
                break;
            case ' ':
                this.doType(32);
                break;
            case '0':
                this.doType(48);
                break;
            case '1':
                this.doType(49);
                break;
            case '2':
                this.doType(50);
                break;
            case '3':
                this.doType(51);
                break;
            case '4':
                this.doType(52);
                break;
            case '5':
                this.doType(53);
                break;
            case '6':
                this.doType(54);
                break;
            case '7':
                this.doType(55);
                break;
            case '8':
                this.doType(56);
                break;
            case '9':
                this.doType(57);
                break;
            case 'A':
                this.doType(16, 65);
                break;
            case 'B':
                this.doType(16, 66);
                break;
            case 'C':
                this.doType(16, 67);
                break;
            case 'D':
                this.doType(16, 68);
                break;
            case 'E':
                this.doType(16, 69);
                break;
            case 'F':
                this.doType(16, 70);
                break;
            case 'G':
                this.doType(16, 71);
                break;
            case 'H':
                this.doType(16, 72);
                break;
            case 'I':
                this.doType(16, 73);
                break;
            case 'J':
                this.doType(16, 74);
                break;
            case 'K':
                this.doType(16, 75);
                break;
            case 'L':
                this.doType(16, 76);
                break;
            case 'M':
                this.doType(16, 77);
                break;
            case 'N':
                this.doType(16, 78);
                break;
            case 'O':
                this.doType(16, 79);
                break;
            case 'P':
                this.doType(16, 80);
                break;
            case 'Q':
                this.doType(16, 81);
                break;
            case 'R':
                this.doType(16, 82);
                break;
            case 'S':
                this.doType(16, 83);
                break;
            case 'T':
                this.doType(16, 84);
                break;
            case 'U':
                this.doType(16, 85);
                break;
            case 'V':
                this.doType(16, 86);
                break;
            case 'W':
                this.doType(16, 87);
                break;
            case 'X':
                this.doType(16, 88);
                break;
            case 'Y':
                this.doType(16, 89);
                break;
            case 'Z':
                this.doType(16, 90);
                break;
            case 'a':
                this.doType(65);
                break;
            case 'b':
                this.doType(66);
                break;
            case 'c':
                this.doType(67);
                break;
            case 'd':
                this.doType(68);
                break;
            case 'e':
                this.doType(69);
                break;
            case 'f':
                this.doType(70);
                break;
            case 'g':
                this.doType(71);
                break;
            case 'h':
                this.doType(72);
                break;
            case 'i':
                this.doType(73);
                break;
            case 'j':
                this.doType(74);
                break;
            case 'k':
                this.doType(75);
                break;
            case 'l':
                this.doType(76);
                break;
            case 'm':
                this.doType(77);
                break;
            case 'n':
                this.doType(78);
                break;
            case 'o':
                this.doType(KeyEvent.VK_O);
                break;
            case 'p':
                this.doType(80);
                break;
            case 'q':
                this.doType(81);
                break;
            case 'r':
                this.doType(82);
                break;
            case 's':
                this.doType(83);
                break;
            case 't':
                this.doType(84);
                break;
            case 'u':
                this.doType(85);
                break;
            case 'v':
                this.doType(86);
                break;
            case 'w':
                this.doType(87);
                break;
            case 'x':
                this.doType(88);
                break;
            case 'y':
                this.doType(89);
                break;
            case 'z':
                this.doType(90);
        }

    } catch (Exception var5) {
            System.out.println(var5);

}

}

    private boolean isWindows() {
        return OS.contains("win");
    }

    private boolean isMac() {
        return OS.contains("mac");
    }
    private void writeDelayIfNeeded() {
        if (this.writeDelay > 0) {
            this.sleep(this.writeDelay);
        }
    }

    private void sleep(int milliseconds) {
        try {
            Thread.sleep((long)milliseconds);
        } catch (InterruptedException var3) {
        }

    }
    private void doType(int... keyCodes) {
        this.doType(keyCodes, 0, keyCodes.length);
    }

    public void doType(int keyCode) {
        this.getRobot().keyPress(keyCode);
        this.writeDelayIfNeeded();
        this.getRobot().keyRelease(keyCode);
    }

    private boolean doType(int[] keyCodes, int offset, int length) {
        if (length == 0) {
            return false;
        } else {
            this.getRobot().keyPress(keyCodes[offset]);
            this.writeDelayIfNeeded();
            if (this.doType(keyCodes, offset + 1, length - 1)) {
                this.writeDelayIfNeeded();
            }

            this.getRobot().keyRelease(keyCodes[offset]);
            return true;
        }
    }
    private Robot getRobot() {
        if (this.robot == null) {
            try {
                this.robot = new Robot();
                return this.robot;
            } catch (Exception var2) {
                return null;
            }
        } else {
            return this.robot;
        }
    }
}
