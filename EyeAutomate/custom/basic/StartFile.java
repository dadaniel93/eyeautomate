package custom.basic;

import java.awt.FileDialog;
import java.awt.Frame;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import eyeautomate.ScriptRunner;

public class StartFile
{
	private static final String COMMAND="StartFile";
	private static final String ICON_FILENAME="icons/console2.png";
	private Properties scriptParameters;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		this.scriptParameters=scriptParameters;
		List<String> commandLine=new ArrayList<String>(Arrays.asList(commandParameters));
		commandLine.add(0, COMMAND);
		return performCommand("", COMMAND, 1, commandLine);
	}

	public String getTooltip()
	{
		return "<html>Launches a file with the associated application<br/>Usage: <br/>"+COMMAND+" FilePath</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		FileDialog fd = new FileDialog((Frame)null, "Select application or file", FileDialog.LOAD);
		fd.setVisible(true);
		if (fd.getFile() != null)
		{
			String filename = fd.getDirectory() + fd.getFile();
			return COMMAND+" \"" + filename + "\"";
		}
		return null;
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/basiccommands.html";
	}

	private void errorLogMessage(String scriptFilename, String text, int lineNo)
	{
		scriptParameters.put("Error", text);
	}

	private boolean performCommand(String scriptFilename, String command, int lineNo, List<String> commandLine)
	{
		if (commandLine.size() < 2)
		{
			errorLogMessage(scriptFilename, "Missing parameter in StartFile command. Usage: StartFile FilePath", lineNo);
			return false;
		}
		else
		{
			String commandString = commandLine.get(1);
			ScriptRunner.startFile(commandString);
		}

		return true;
	}
}
