package custom.basic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class Paste extends EyeCommand
{
	private static final String COMMAND="Paste";
	private static final String ICON_FILENAME="icons/paste2.png";
	private Properties scriptParameters;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		this.scriptParameters=scriptParameters;
		List<String> commandLine=new ArrayList<String>(Arrays.asList(commandParameters));
		commandLine.add(0, COMMAND);
		return performCommand("", COMMAND, 1, commandLine);
	}

	public String getTooltip()
	{
		return "<html>Paste text using the clipboard<br/>Usage: <br/>"+COMMAND+" Text</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<String: Enter text>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	private void errorLogMessage(String scriptFilename, String text, int lineNo)
	{
		scriptParameters.put("Error", text);
	}

	private boolean performCommand(String scriptFilename, String command, int lineNo, List<String> commandLine)
	{
		if (commandLine.size() < 2)
		{
			errorLogMessage(scriptFilename, "Missing parameter in Type command. Usage: "+COMMAND+" Text", lineNo);
			return false;
		}
		else
		{
			// Concatenate all parameters
			StringBuffer textBuffer=new StringBuffer();
			for (int i = 1; i < commandLine.size(); i++)
			{
				if(i>1)
				{
					textBuffer.append(" ");
				}
				String textParam = commandLine.get(i);
				textBuffer.append(scriptRunner.decrypt(textParam));
			}
			String text = textBuffer.toString();
			eye.paste(text);
		}

		return true;
	}
}
