package custom.basic;

import java.util.Properties;

import eyeautomate.CustomCommand;
import eyeautomate.ScriptRunner;

public class WhileNot
{
	private static final String COMMAND="WhileNot";
	private static final String ICON_FILENAME="icons/while2.png";
	ScriptRunner scriptRunner=null;
	
	public Boolean executeCommand(String[] commandNonReplacedParameters, String[] commandParameters, Properties scriptParameters)
	{
		if(commandParameters.length<1)
		{
			scriptParameters.put("Error", "Missing parameter in command. Usage: "+COMMAND+" Image/Expression");
			return false;
		}

		String param=commandParameters[0];
		String[] parameters=commandParameters;
		if(scriptRunner.isExpression(param))
		{
			// Is an expression - use non replaced parameters
			parameters=commandNonReplacedParameters;
			scriptParameters.setProperty("Iteration", "0");
		}
		CustomCommand command=new CustomCommand(COMMAND, parameters);
		scriptRunner.setBlockCommand(command);
		scriptRunner.increaseCommandLevel();
		return true;
	}

	public String getTooltip()
	{
		return "<html>Perform block while image does not exist<br/>Usage: <br/>"+COMMAND+" Image [Area]<br/>"+COMMAND+" Expression</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		return COMMAND+" <Image>...EndWhile";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/basiccommands.html";
	}

	public String getBeginCommand()
	{
		return COMMAND;
	}

	public String getEndCommand()
	{
		return "EndWhile";
	}

	public void setScriptRunner(ScriptRunner scriptRunner)
	{
		this.scriptRunner=scriptRunner;
	}
}
