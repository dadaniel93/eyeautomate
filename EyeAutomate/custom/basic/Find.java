package custom.basic;

import java.awt.Color;
import java.awt.Font;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import eye.Match;
import eyeautomate.ScriptRunner;

public class Find extends EyeCommand
{
	private static final String COMMAND="Find";
	private static final String ICON_FILENAME="icons/deselect2.png";
	private Properties scriptParameters;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		this.scriptParameters=scriptParameters;
		List<String> commandLine=new ArrayList<String>(Arrays.asList(commandParameters));
		commandLine.add(0, COMMAND);
		boolean passed=performCommand("", COMMAND, 1, commandLine);
		if(passed)
		{
			scriptRunner.setScreenshot(eye.captureCurrentLocation());
		}
		else
		{
			scriptRunner.setScreenshot(eye.getLatestScreenCapture());
		}
		return passed;
	}

	public String getTooltip()
	{
		return "<html>Locates an image<br/>Returns all matching locations<br/>Usage:<br/>"+COMMAND+" Image [Location/Area]<br/>"+COMMAND+" Text</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error", "MatchCount", "MatchLocation1", "MatchLocation2", "MatchLocation3", "MatchLocation4", "MatchLocation5", "MatchLocation6", "MatchLocation7", "MatchLocation8", "MatchLocation9"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<Image>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/basiccommands.html";
	}

	/**
	 * Determine if s is an integer (may begin with a - or + sign)
	 * @param s
	 * @return true if s is an integer
	 */
	private static boolean isIntString(String s)
	{
		String text = s.trim();
		for (int i = 0; i < text.length(); i++)
		{
			char c = text.charAt(i);
			if (i==0)
			{
				// First char may contain - or +
				if (!(Character.isDigit(c) || c == '-' || c == '+'))
				{
					return false;
				}
			}
			else
			{
				if (!Character.isDigit(c))
				{
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Converts to s to an int
	 * @return An int or 0 if failed
	 */
	private static int string2Int(String s)
	{
		return string2Int(s, 0);
	}

	/**
	 * Converts to s to an int
	 * @return An int or an alternative int if failed
	 */
	private static int string2Int(String s, int otherwise)
	{
		String text = s.trim();
		if(s.length()==0)
		{
			// Blank
			return 0;
		}
		char c = text.charAt(0);
		if (c == '+')
		{
			// Remove + sign
			text=text.substring(1);
		}
		try
		{
			return Integer.parseInt(text);
		}
		catch (Exception e)
		{
			// Couldn't convert to int
			return otherwise;
		}
	}

	private BufferedImage loadImage(String filename)
	{
		return scriptRunner.loadImage(filename);
	}

	private void errorLogMessage(String scriptFilename, String text, int lineNo)
	{
		scriptParameters.put("Error", text);
	}

	private boolean performCommand(String scriptFilename, String command, int lineNo, List<String> commandLine)
	{
		if (commandLine.size() < 2)
		{
			errorLogMessage(scriptFilename, "Missing parameter in "+command+" command. Usage: "+command+" [Image/Text] [PercentX|RelativeX|ImageX] [PercentY|RelativeY|ImageY] [Width] [Height]", lineNo);
			return false;
		}
		else
		{
			if (commandLine.size() == 4)
			{
				// Image and relative position
				String filename = commandLine.get(1);
				String xs = commandLine.get(2);
				String ys = commandLine.get(3);

				BufferedImage imageToFind = loadImage(filename);
				if (imageToFind == null)
				{
					errorLogMessage(scriptFilename, "Image not found", lineNo);
					return false;
				}

				List<Match> matches=eye.findImages(imageToFind);
				if(matches==null)
				{
					errorLogMessage(scriptFilename, command+" failed, image not found", lineNo);
					return false;
				}
				scriptParameters.put("MatchCount", ""+matches.size());
				int imageNo=1;
				for(Match match:matches)
				{
					Point location=match.getRelativeLocation(xs, ys);
					scriptParameters.put("MatchLocation"+imageNo++, (int)location.getX()+" "+(int)location.getY());
				}
			}
			else if (commandLine.size() == 6)
			{
				String filename = commandLine.get(1);
				if(!(isIntString(commandLine.get(2)) && isIntString(commandLine.get(3)) && isIntString(commandLine.get(4)) && isIntString(commandLine.get(5))))
				{
					errorLogMessage(scriptFilename, "Specify coordinates and size using integer values", lineNo);
					return false;
				}

				BufferedImage imageToFind = loadImage(filename);
				if (imageToFind == null)
				{
					errorLogMessage(scriptFilename, "Image not found", lineNo);
					return false;
				}

				// Create a target area to check
				int x = string2Int(commandLine.get(2));
				int y = string2Int(commandLine.get(3));
				int width = string2Int(commandLine.get(4));
				int height = string2Int(commandLine.get(5));
				Rectangle targetArea=new Rectangle(x, y, width, height);

				List<Match> matches=eye.findImages(imageToFind, targetArea);
				if(matches==null)
				{
					errorLogMessage(scriptFilename, command+" failed, image not found", lineNo);
					return false;
				}
				scriptParameters.put("MatchCount", ""+matches.size());
				int imageNo=1;
				for(Match match:matches)
				{
					int relativePosX=x+width/2;
					int relativePosY=y+height/2;
					Point location=match.getRelativeLocation(relativePosX, relativePosY);
					scriptParameters.put("MatchLocation"+imageNo++, (int)location.getX()+" "+(int)location.getY());
				}
			}
			else if (ScriptRunner.isImage(commandLine.get(1)))
			{
				// One image
				String filename = commandLine.get(1);
				BufferedImage imageToFind = loadImage(filename);
				if (imageToFind == null)
				{
					errorLogMessage(scriptFilename, "Image not found", lineNo);
					return false;
				}

				List<Match> matches=eye.findImages(imageToFind);
				if(matches==null)
				{
					errorLogMessage(scriptFilename, command+" failed, image not found", lineNo);
					return false;
				}
				scriptParameters.put("MatchCount", ""+matches.size());
				int imageNo=1;
				for(Match match:matches)
				{
					Point location=match.getCenterLocation();
					scriptParameters.put("MatchLocation"+imageNo++, (int)location.getX()+" "+(int)location.getY());
				}
			}
			else
			{
				// One text
				String text = commandLine.get(1);
				String fontName=scriptParameters.getProperty("FontName", "Consolas");
				String fontStyleStr=scriptParameters.getProperty("FontStyle", "Plain");
				int fontStyle=Font.PLAIN;
				if (fontStyleStr.equalsIgnoreCase("bold"))
				{
					fontStyle=Font.BOLD;
				}
				else if (fontStyleStr.equalsIgnoreCase("italic"))
				{
					fontStyle=Font.ITALIC;
				}
				int fontSize=string2Int(scriptParameters.getProperty("FontSize", "11"));
				Font font=eye.createFont(fontName, fontStyle, fontSize, false, false);
				BufferedImage imageToFind = eye.createImageFromText(text, font, Color.BLACK, Color.WHITE);
				if (imageToFind == null)
				{
					errorLogMessage(scriptFilename, "Failed to create image", lineNo);
					return false;
				}

				List<Match> matches=eye.findImages(imageToFind);
				if(matches==null)
				{
					errorLogMessage(scriptFilename, command+" failed, image not found", lineNo);
					return false;
				}
				scriptParameters.put("MatchCount", ""+matches.size());
				int imageNo=1;
				for(Match match:matches)
				{
					Point location=match.getCenterLocation();
					scriptParameters.put("MatchLocation"+imageNo++, (int)location.getX()+" "+(int)location.getY());
				}
			}

			return true;
		}
	}
	
	public void stopScript()
	{
		eye.setStop(true);
	}

	public void startScript()
	{
		eye.resetImageRecognitionSettings();
	}
}
