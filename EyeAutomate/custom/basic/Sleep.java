package custom.basic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import eyeautomate.ScriptRunner;

public class Sleep
{
	private static final String COMMAND="Sleep";
	private static final String ICON_FILENAME="icons/timer2.png";
	private ScriptRunner scriptRunner=null;
	private Properties scriptParameters;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		this.scriptParameters=scriptParameters;
		List<String> commandLine=new ArrayList<String>(Arrays.asList(commandParameters));
		commandLine.add(0, COMMAND);
		boolean passed=performCommand("", COMMAND, 1, commandLine);
		return passed;
	}

	public String getTooltip()
	{
		return "<html>Delay a number of milliseconds<br/>Usage:<br/>"+COMMAND+" Milliseconds</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		return COMMAND+" <String: No milliseconds to sleep>";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/basiccommands.html";
	}

	/**
	 * Determine if s is an integer (may begin with a - or + sign)
	 * @param s
	 * @return true if s is an integer
	 */
	private static boolean isIntString(String s)
	{
		String text = s.trim();
		for (int i = 0; i < text.length(); i++)
		{
			char c = text.charAt(i);
			if (i==0)
			{
				// First char may contain - or +
				if (!(Character.isDigit(c) || c == '-' || c == '+'))
				{
					return false;
				}
			}
			else
			{
				if (!Character.isDigit(c))
				{
					return false;
				}
			}
		}
		return true;
	}

	public void setScriptRunner(ScriptRunner scriptRunner)
	{
		this.scriptRunner=scriptRunner;
	}

	/**
	 * Converts to s to an int
	 * @return An int or 0 if failed
	 */
	private static int string2Int(String s)
	{
		return string2Int(s, 0);
	}

	/**
	 * Converts to s to an int
	 * @return An int or an alternative int if failed
	 */
	private static int string2Int(String s, int otherwise)
	{
		String text = s.trim();
		if(s.length()==0)
		{
			// Blank
			return 0;
		}
		char c = text.charAt(0);
		if (c == '+')
		{
			// Remove + sign
			text=text.substring(1);
		}
		try
		{
			return Integer.parseInt(text);
		}
		catch (Exception e)
		{
			// Couldn't convert to int
			return otherwise;
		}
	}

	private void errorLogMessage(String scriptFilename, String text, int lineNo)
	{
		scriptParameters.put("Error", text);
	}

	private boolean performCommand(String scriptFilename, String command, int lineNo, List<String> commandLine)
	{
		if (commandLine.size() == 2)
		{
			String filename = commandLine.get(1);
			if (isIntString(filename))
			{
				// Wait a number of milliseconds
				int noMilliseconds = string2Int(filename);
				if (noMilliseconds < 0 || noMilliseconds > 1000000)
				{
					errorLogMessage(scriptFilename, "Specify a Wait between 0 and 1000000 milliseconds", lineNo);
					return false;
				}
				else
				{
					wait(noMilliseconds);
				}
			}
			else
			{
				errorLogMessage(scriptFilename, "Specify a Wait between 0 and 1000000 milliseconds", lineNo);
				return false;
			}
		}
		else
		{
			errorLogMessage(scriptFilename, "Missing parameter in Sleep command. Usage: Sleep Milliseconds", lineNo);
			return false;
		}

		return true;
	}

	/**
	 * Wait for a number of milliseconds
	 * @param noMilliSeconds
	 */
	public void wait(int noMilliSeconds)
	{
		if (noMilliSeconds > 0)
		{
			int remaining=noMilliSeconds;
			while(true)
			{
				try
				{
					if(scriptRunner.isStop())
					{
						// Stop executing
						return;
					}
					if(remaining>=1000)
					{
						// Wait one second
						remaining-=1000;
						sleep(1000);
					}
					else
					{
						// Wait less than one second
						sleep(1000);
						return;
					}
				}
				catch (Exception e)
				{
				}
			}
		}
	}

	/**
	 * Delay the Thread for a number of milliseconds
	 * @param milliseconds
	 */
	private void sleep(int milliseconds)
	{
		try
		{
			Thread.sleep(milliseconds);
		}
		catch (InterruptedException e)
		{
		}
	}
}
