package custom.settings;

import java.util.Properties;

import custom.basic.EyeCommand;
import eye.Eye;

public class Recognition extends EyeCommand
{
	private static final String COMMAND="Recognition";
	private static final String ICON_FILENAME="icons/analysis2.png";

	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		if(commandParameters.length<1)
		{
			scriptParameters.put("Error", "Missing parameter in command. Usage: "+COMMAND+" Either/Both/Pixels/Vectors");
			return false;
		}
		String param = commandParameters[0].trim();
		if(!setRecognition(param))
		{
			scriptParameters.put("Error", "Invalid image recognition algorithm, specify Either, Both, Pixels or Vectors");
			return false;
		}

		return true;
	}

	public String getTooltip()
	{
		return "<html>Select image recognition algorithm<br/>Usage:<br/>"+COMMAND+" Either/Both/Pixels/Vectors</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<String: Select image recognition algorithm=Either|Both|Pixels|Vectors>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	private boolean setRecognition(String recognition)
	{
		if("either".equalsIgnoreCase(recognition))
		{
			eye.setRecognition(Eye.Recognition.EITHER);
		}
		else if("both".equalsIgnoreCase(recognition))
		{
			eye.setRecognition(Eye.Recognition.BOTH);
		}
		else if("pixels".equalsIgnoreCase(recognition))
		{
			eye.setRecognition(Eye.Recognition.PIXELS);
		}
		else if("vectors".equalsIgnoreCase(recognition))
		{
			eye.setRecognition(Eye.Recognition.VECTORS);
		}
		else
		{
			return false;
		}
		return true;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/settingscommands.html";
	}
}
