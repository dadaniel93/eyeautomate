package custom.extended;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import eyeautomate.HttpServiceCaller;
import eyeautomate.ScriptRunner;

public class CallRemote
{
	private static final String COMMAND="CallRemote";
	private static final String ICON_FILENAME="icons/call2.png";
	private ScriptRunner scriptRunner=null;
	private Properties scriptParameters;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		this.scriptParameters=scriptParameters;
		List<String> commandLine=new ArrayList<String>(Arrays.asList(commandParameters));
		commandLine.add(0, COMMAND);
		boolean passed=performCommand("", COMMAND, 1, commandLine);
		if(!passed)
		{
			scriptRunner.setScreenshot(scriptRunner.getLatestScreenCapture());
		}
		return passed;
	}

	public String getTooltip()
	{
		return "<html>Calls a remote script<br/>Usage: <br/>"+COMMAND+" Script<br/>"+COMMAND+" Address</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<String: Enter the remote address of the script=http://localhost:1234/>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public void setScriptRunner(ScriptRunner scriptRunner)
	{
		this.scriptRunner=scriptRunner;
	}

	private void errorLogMessage(String scriptFilename, String text, int lineNo)
	{
		scriptParameters.put("Error", text);
	}

	private boolean performCommand(String scriptFilename, String command, int lineNo, List<String> commandLine)
	{
		if (commandLine.size() < 2)
		{
			errorLogMessage(scriptFilename, "Missing parameter in CallRemote command. Usage: CallRemote Address", lineNo);
			return false;
		}
		else
		{
			String address=commandLine.get(1);
			HttpServiceCaller service=new HttpServiceCaller();
			String response=service.executeGetRequest(address);
			if(response==null)
			{
				errorLogMessage(scriptFilename, "Failed to call: "+address, lineNo);
				return false;
			}
			else
			{
				if(response.startsWith("<html>Failed:"))
				{
					// Failed
					ScriptRunner.setRemoteReport(extractUrlFromLink(response.substring(13).trim()));
					errorLogMessage(scriptFilename, "Remote script failed", lineNo);
					return false;
				}
				else if(response.startsWith("<html>Completed:"))
				{
					// Completed
					ScriptRunner.setRemoteReport(extractUrlFromLink(response.substring(16).trim()));
				}
				else
				{
					errorLogMessage(scriptFilename, "Unknown response from remote script", lineNo);
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Extracts the url from a link, example: <a href='/reports/scriptsuite.txt_2012-11-27_21-32-26.htm'>
	 * @param link
	 * @return
	 */
	private String extractUrlFromLink(String link)
	{
		int start=link.indexOf("href='");
		int end=link.indexOf("'>");
		if(start>=0 && end>0)
		{
			return link.substring(start+6, end);
		}
		return null;
	}
}
