package custom.extended;

import java.io.File;
import java.util.Properties;

import eyeautomate.ScriptRunner;

public class DeleteCapture
{
	private static final String COMMAND="DeleteCapture";
	private static final String ICON_FILENAME="icons/delete2.png";
	ScriptRunner scriptRunner=null;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		String capturedImage=scriptParameters.getProperty("CapturedImage");
		if (capturedImage==null)
		{
			scriptParameters.put("Error", "No captured image");
			return false;
		}

		String fileName = scriptRunner.addRootFolder(capturedImage);
		
		File f = new File(fileName);

		// Make sure the file or directory exists and isn't write protected
		if (!f.exists())
		{
			scriptParameters.put("Error", "No such file or directory: "+fileName);
			return false;
		}

		if (!f.canWrite())
		{
			scriptParameters.put("Error", "Write protected: "+fileName);
			return false;
		}

		// If it is a directory, make sure it is empty
		if (f.isDirectory())
		{
			String[] files = f.list();
			if (files.length > 0)
			{
				scriptParameters.put("Error", "Directory not empty: "+fileName);
				return false;
			}
		}

		if (!f.delete())
		{
			scriptParameters.put("Error", "Failed to delete: "+fileName);
			return false;
		}

		return true;
	}

	public String getTooltip()
	{
		return "<html>Deletes the latest image capture<br/>Usage:<br/>"+COMMAND+"</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public void setScriptRunner(ScriptRunner scriptRunner)
	{
		this.scriptRunner=scriptRunner;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/extendedcommands.html";
	}
}
