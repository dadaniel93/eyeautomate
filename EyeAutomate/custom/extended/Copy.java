package custom.extended;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import custom.basic.EyeCommand;

public class Copy extends EyeCommand
{
	private static final String COMMAND="Copy";
	private static final String ICON_FILENAME="icons/copy2.png";
	private Properties scriptParameters;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		this.scriptParameters=scriptParameters;
		List<String> commandLine=new ArrayList<String>(Arrays.asList(commandParameters));
		commandLine.add(0, COMMAND);
		return performCommand("", COMMAND, 1, commandLine);
	}

	public String getTooltip()
	{
		return "<html>Copy text to the clipboard<br/>Usage: <br/>"+COMMAND+" Text</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<String: Enter text>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	private void errorLogMessage(String scriptFilename, String text, int lineNo)
	{
		scriptParameters.put("Error", text);
	}

	private boolean performCommand(String scriptFilename, String command, int lineNo, List<String> commandLine)
	{
		if (commandLine.size() < 2)
		{
			errorLogMessage(scriptFilename, "Missing parameter in Type command. Usage: "+COMMAND+" Text", lineNo);
			return false;
		}
		else
		{
			String text = commandLine.get(1);
			eye.copy(text);
		}

		return true;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/extendedcommands.html";
	}
}
