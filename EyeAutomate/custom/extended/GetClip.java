package custom.extended;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class GetClip
{
	private static final String COMMAND="GetClip";
	private static final String ICON_FILENAME="icons/analysis2.png";
	private Properties scriptParameters;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		this.scriptParameters=scriptParameters;
		List<String> commandLine=new ArrayList<String>(Arrays.asList(commandParameters));
		commandLine.add(0, COMMAND);
		return performCommand("", COMMAND, 1, commandLine);
	}

	public String getTooltip()
	{
		return "<html>Get a clip from the clipboard<br/>Usage: <br/>"+COMMAND+"</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error", "Clip"};
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	private void errorLogMessage(String scriptFilename, String text, int lineNo)
	{
		scriptParameters.put("Error", text);
	}

	private boolean performCommand(String scriptFilename, String command, int lineNo, List<String> commandLine)
	{
		try
		{
			Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
			Transferable cliptran = clipboard.getContents(this);
			String sel = (String) cliptran.getTransferData(DataFlavor.stringFlavor);
			scriptParameters.put("Clip", sel);
		}
		catch (Exception e)
		{
			errorLogMessage(scriptFilename, "Failed to get clip from clipboard", lineNo);
			return false;
		}

		return true;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/extendedcommands.html";
	}
}
