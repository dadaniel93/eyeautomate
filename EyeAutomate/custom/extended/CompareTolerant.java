package custom.extended;

import java.awt.Color;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Properties;

import custom.basic.EyeCommand;

public class CompareTolerant extends EyeCommand
{
	private static final String COMMAND="CompareTolerant";
	private static final String ICON_FILENAME="icons/question2.png";

	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		if(commandParameters.length<1)
		{
			scriptParameters.put("Error", "Missing parameter in command. Usage: "+COMMAND+" Image");
			return false;
		}

		String text=commandParameters[0].trim();

		try
		{
			BufferedImage image=scriptRunner.loadImage(text);
			if(image!=null)
			{
				List<Rectangle> rectangles=eye.compareTolerant(image);
				if(rectangles.size()>0)
				{
					BufferedImage imageDiff=eye.createRectangleImage(image, rectangles, Color.red);
					scriptRunner.setScreenshot(imageDiff);
					scriptParameters.put("Error", "Image not identical");
					return false;
				}
				return true;
			}
			else
			{
				scriptParameters.put("Error", "Failed to load image");
				return false;
			}
		}
		catch(Exception e)
		{
			scriptParameters.put("Error", "Exception: "+e.toString());
			return false;
		}
	}

	public String getTooltip()
	{
		return "<html>Locates and compares an image inside the browser<br/>Usage:<br/>CompareImageTolerant Image</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<Image>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/extendedcommands.html";
	}
}
