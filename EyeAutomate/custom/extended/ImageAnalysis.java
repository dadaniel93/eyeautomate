package custom.extended;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import custom.basic.EyeCommand;
import eye.Eye;

public class ImageAnalysis extends EyeCommand
{
	private static final String COMMAND="ImageAnalysis";
	private static final String ICON_FILENAME="icons/analysis2.png";
	private Properties scriptParameters;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		this.scriptParameters=scriptParameters;
		List<String> commandLine=new ArrayList<String>(Arrays.asList(commandParameters));
		commandLine.add(0, COMMAND);
		boolean passed=performCommand("", COMMAND, 1, commandLine);
		if(passed)
		{
			scriptRunner.setScreenshot(eye.captureCurrentLocation());
		}
		else
		{
			scriptRunner.setScreenshot(eye.getLatestScreenCapture());
		}
		return passed;
	}

	public String getTooltip()
	{
		return "<html>Creates an analysis image<br/>Usage:<br/>"+COMMAND+" Image</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error", "AnalysisImage"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<Image>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}
	
	/**
	 * Determine if s is an integer (may begin with a - or + sign)
	 * @param s
	 * @return true if s is an integer
	 */
	private static boolean isIntString(String s)
	{
		String text = s.trim();
		for (int i = 0; i < text.length(); i++)
		{
			char c = text.charAt(i);
			if (i==0)
			{
				// First char may contain - or +
				if (!(Character.isDigit(c) || c == '-' || c == '+'))
				{
					return false;
				}
			}
			else
			{
				if (!Character.isDigit(c))
				{
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Converts to s to an int
	 * @return An int or 0 if failed
	 */
	private static int string2Int(String s)
	{
		return string2Int(s, 0);
	}

	/**
	 * Converts to s to an int
	 * @return An int or an alternative int if failed
	 */
	private static int string2Int(String s, int otherwise)
	{
		String text = s.trim();
		if(s.length()==0)
		{
			// Blank
			return 0;
		}
		char c = text.charAt(0);
		if (c == '+')
		{
			// Remove + sign
			text=text.substring(1);
		}
		try
		{
			return Integer.parseInt(text);
		}
		catch (Exception e)
		{
			// Couldn't convert to int
			return otherwise;
		}
	}

	private BufferedImage loadImage(String filename)
	{
		return scriptRunner.loadImage(filename);
	}

	private void errorLogMessage(String scriptFilename, String text, int lineNo)
	{
		scriptParameters.put("Error", text);
	}

	private boolean performCommand(String scriptFilename, String command, int lineNo, List<String> commandLine)
	{
		if (commandLine.size() < 2)
		{
			errorLogMessage(scriptFilename, "Missing parameter in "+command+" command. Usage: "+command+" Image", lineNo);
			return false;
		}
		else
		{
			if (commandLine.size() == 6)
			{
				String filename = commandLine.get(1);
				if(!(isIntString(commandLine.get(2)) && isIntString(commandLine.get(3)) && isIntString(commandLine.get(4)) && isIntString(commandLine.get(5))))
				{
					errorLogMessage(scriptFilename, "Specify size using integer values", lineNo);
					return false;
				}

				BufferedImage imageToFind = loadImage(filename);
				if (imageToFind == null)
				{
					errorLogMessage(scriptFilename, "Image/Text not found", lineNo);
					return false;
				}

				// Create a target area to check
				int x = string2Int(commandLine.get(2));
				int y = string2Int(commandLine.get(3));
				int width = string2Int(commandLine.get(4));
				int height = string2Int(commandLine.get(5));
				Rectangle targetArea=new Rectangle(x, y, width, height);

				BufferedImage analysisImage=eye.createImageAnalysis(imageToFind, targetArea);
				if(analysisImage!=null)
				{
					String imageFolder=scriptRunner.getImageFolder();
					String analysisFilename=imageFolder+"/imageAnalysis.png";
					Eye.savePngImage(analysisImage, analysisFilename);
					scriptParameters.put("AnalysisImage", analysisFilename);
				}
			}
			else
			{
				// One image
				String filename = commandLine.get(1);
				BufferedImage imageToFind = loadImage(filename);
				if (imageToFind == null)
				{
					errorLogMessage(scriptFilename, "Image/Text not found", lineNo);
					return false;
				}

				BufferedImage analysisImage=eye.createImageAnalysis(imageToFind);
				if(analysisImage!=null)
				{
					String imageFolder=scriptRunner.getImageFolder();
					String analysisFilename=imageFolder+"/imageAnalysis.png";
					Eye.savePngImage(analysisImage, analysisFilename);
					scriptParameters.put("AnalysisImage", analysisFilename);
				}
			}
			
			return true;
		}
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/extendedcommands.html";
	}
}
