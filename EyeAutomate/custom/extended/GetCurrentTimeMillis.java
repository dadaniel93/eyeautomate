package custom.extended;
import java.util.Properties;



public class GetCurrentTimeMillis
{
    private static final String COMMAND="GetCurrentTimeMillis";
    private static final String ICON_FILENAME="icons/analysis2.png";


    public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
    {

        if(commandParameters.length<1)
        {
            scriptParameters.put("Error", "Missing parameter in command.");
            return false;
        }

        String parameter=commandParameters[0];
        long parameterValue = System.currentTimeMillis();
        scriptParameters.put(parameter, Long.toString(parameterValue));

        return true;
    }


    public String getTooltip()
    {
        return "<html>Returns the current time in milliseconds<br/>Usage: <br/>"+COMMAND+"</html>";
    }

    public String[] getParameters()
    {
        return new String[]{"Error"};
    }

    public String getIconFilename()
    {
        return ICON_FILENAME;
    }

    public String getHelp()
    {
        return "http://www.eyeautomate.com/extendedcommands.html";

    }


}




