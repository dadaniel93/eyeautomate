package custom.extended;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;



public class CompareResults {


    private static final String COMMAND="CompareResults";
    private static final String ICON_FILENAME="icons/analysis2.png";
    private static final String SLACK_HOOKS = "https://hooks.slack.com/services/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";
    private static  String EXPECTED_RESULTS="";
    private static  String EVENTS_LOGS="";
    private static  String TEST_NAME ="";
    private static  String MESSAGE ="TEST";
    private static  Boolean WARNING = false;


    public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
    {

        if(commandParameters.length<3)
        {
            scriptParameters.put("Error", "Missing parameter in command.");
            return false;
        }
        List<String> commandLine=new ArrayList<String>(Arrays.asList(commandParameters));
        EXPECTED_RESULTS = commandLine.get(0);
        EVENTS_LOGS = commandLine.get(1);
        TEST_NAME = commandLine.get(2);
        ProcessExpectedResults();
       // MESSAGE ="";
        return true;
    }

    public String getTooltip()
    {
        return "<html>Sending \" Start\" message to Slack br/>Usage: <br/>"+COMMAND+"</html>";
    }

    public String[] getParameters()
    {
        return new String[]{"Error"};
    }

    public String getIconFilename()
    {
        return ICON_FILENAME;
    }

    public String getHelp()
    {
        return "http://www.eyeautomate.com/extendedcommands.html";

    }
    public void ProcessExpectedResults(){

        //EXPECTED_RESULTS = "/Users/ddahlin/Downloads/REG-3202_Results.txt";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        String EventName;
        int ExpectedCount;

        try {

            br = new BufferedReader(new FileReader(EXPECTED_RESULTS));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] ExpectedResults = line.split(cvsSplitBy);
                EventName = ExpectedResults[0];
                ExpectedCount = Integer.valueOf(ExpectedResults[1]);
                CompareResults(EventName, ExpectedCount);
            }
            if (WARNING) {
                sendSlackMessage(MESSAGE);
                WARNING = false;
                MESSAGE = "";

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


    }

    public  void CompareResults(String EventName, int ExpectedCount) {

        try {
            int actualResults = searchCount(EventName);

            if (actualResults!= ExpectedCount) {
                MESSAGE = MESSAGE + ""+EventName+" dispatched " + actualResults+" time(s) and doesn't match expected result " + ExpectedCount + "\\n";
                WARNING = true;

            }

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }


    public  int searchCount(String EventName) throws FileNotFoundException {
        int count = 0;
       // EVENTS_LOGS = "/Users/ddahlin/Downloads/REG-3202_inapp.csv";

        try {
            BufferedReader lineParser = new BufferedReader(new FileReader(EVENTS_LOGS));
            String line;
            while ((line = lineParser.readLine()) != null) {
                StringTokenizer tokenizer = new StringTokenizer(line, ",");
                while (tokenizer.hasMoreTokens()) {
                    if (tokenizer.nextToken().contains(EventName))
                        count++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return count;
    }

    public void sendSlackMessage(String message){

        HttpClient client = HttpClientBuilder.create().build();
        HttpPost httppost = new HttpPost(SLACK_HOOKS);


        try {
            httppost.addHeader("Content-type", "application/json");

            StringEntity params = new StringEntity("{\"attachments\" : [ {\"fallback\":\"" + TEST_NAME +  " Script Status - Completed\"," +
                    "\"pretext\" : \"" + TEST_NAME +  " Script Status - Completed\",\"color\" : \"#FFA500\", " +
                    "\"fields\":[ {\"title\" : \"Test Status: WARNING\",\"value\": \""+message+"\"  }] }] }","UTF-8");

            params.setContentType("application/json");
            httppost.setEntity(params);

            HttpResponse response = client.execute(httppost);
            System.out.println(response.getStatusLine().getStatusCode());

        } catch(

                IOException ie) {
            ie.printStackTrace();
        }

    }


}
