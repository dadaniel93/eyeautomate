package custom.extended;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import eyeautomate.ScriptRunner;

public class ManualTest
{
	private static final String COMMAND="ManualTest";
	private static final String ICON_FILENAME="icons/chat2.png";
	private ScriptRunner scriptRunner=null;
	private Properties scriptParameters;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		this.scriptParameters=scriptParameters;
		List<String> commandLine=new ArrayList<String>(Arrays.asList(commandParameters));
		commandLine.add(0, COMMAND);
		boolean passed=performCommand("", COMMAND, 1, commandLine);
		if(!passed)
		{
			scriptRunner.setScreenshot(scriptRunner.getLatestScreenCapture());
		}
		return passed;
	}

	public String getTooltip()
	{
		return "<html>Display a manual test dialog<br/>Usage: <br/>"+COMMAND+" Instruction<br/>"+COMMAND+" Instruction Image</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<String: Enter instruction>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public void setScriptRunner(ScriptRunner scriptRunner)
	{
		this.scriptRunner=scriptRunner;
	}

	private void errorLogMessage(String scriptFilename, String text, int lineNo)
	{
		scriptParameters.put("Error", text);
	}

	private boolean performCommand(String scriptFilename, String command, int lineNo, List<String> commandLine)
	{
		if (commandLine.size() < 2)
		{
			errorLogMessage(scriptFilename, "Missing parameter in ManualTest command. Usage: ManualTest Instruction [Instruction|Image]", lineNo);
			return false;
		}
		else
		{
			StringBuffer instruction=new StringBuffer();
			for (int i = 1; i < commandLine.size(); i++)
			{
				String param=commandLine.get(i);
				if(ScriptRunner.isImage(param))
				{
					// An image
					instruction.append("<img src='" + param + "'/>");
				}
				else
				{
					// A text
					instruction.append(param);
				}
			}
			return scriptRunner.manualTest(instruction.toString(), scriptFilename, lineNo);
		}
	}
}
