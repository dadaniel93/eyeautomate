package custom.extended;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import eyeautomate.ScriptRunner;

public class Run
{
	private static final String COMMAND="Run";
	private static final String ICON_FILENAME="icons/call2.png";
	private ScriptRunner scriptRunner=null;
	private Properties scriptParameters;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		this.scriptParameters=scriptParameters;
		List<String> commandLine=new ArrayList<String>(Arrays.asList(commandParameters));
		commandLine.add(0, COMMAND);
		return performCommand("", COMMAND, 1, commandLine);
	}

	public String getTooltip()
	{
		return "<html>Runs another script asynchronously<br/>Usage: <br/>"+COMMAND+" Script<br/>"+COMMAND+" [Iterations] [Parameters] Script</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<Script>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public void setScriptRunner(ScriptRunner scriptRunner)
	{
		this.scriptRunner=scriptRunner;
	}

	private void errorLogMessage(String scriptFilename, String text, int lineNo)
	{
		scriptParameters.put("Error", text);
	}

	private boolean performCommand(String scriptFilename, String command, int lineNo, List<String> commandLine)
	{
		if (commandLine.size() < 2)
		{
			errorLogMessage(scriptFilename, "Missing parameter in Run command. Usage: Run [Iterations] [Parameters] Script", lineNo);
			return false;
		}
		else
		{
			String[] params = new String[commandLine.size() - 1];
			for (int i = 1; i < commandLine.size(); i++)
			{
				params[i - 1] = commandLine.get(i);
			}

			RunThread runThread=new RunThread(params);
			runThread.start();
		}

		return true;
	}

	private class RunThread extends Thread
	{
		private String params[];

		public RunThread(String params[])
		{
			super();
			this.params = params;
		}

		public void run()
		{
			ScriptRunner sr=scriptRunner.createChildScriptRunner();
			sr.runScriptInternal(params);
			scriptRunner.removeChildScriptRunner(sr);
		}
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/extendedcommands.html";
	}
}
