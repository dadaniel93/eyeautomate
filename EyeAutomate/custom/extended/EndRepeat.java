package custom.extended;

import java.io.StringReader;
import java.util.Properties;

import eyeautomate.CSVReader;
import eyeautomate.Command;
import eyeautomate.CustomCommand;
import eyeautomate.ScriptRunner;

public class EndRepeat
{
	private static final String COMMAND="EndRepeat";
	private static final String ICON_FILENAME="icons/repeat2.png";
	private ScriptRunner scriptRunner=null;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		Command blockCommand=scriptRunner.getBlockCommand();
		if(blockCommand!=null && scriptRunner.getCommandLevel()==1)
		{
			if(blockCommand instanceof CustomCommand)
			{
				scriptRunner.decreaseCommandLevel();
				CustomCommand customCommand=(CustomCommand)blockCommand;
				String[] commandLine=customCommand.getCommandParameters();
				String param=commandLine[0];

				if(param.toLowerCase().endsWith(".csv"))
				{
					// Iterate over a CSV file
					try
					{
						CSVReader csvReader = new CSVReader(scriptRunner.addRootFolder(param));
						int iterationNo=1;
						while (csvReader.hasNext())
						{
							if (scriptRunner.isStop())
							{
								// Continue with next operation
								return false;
							}
							scriptParameters.putAll(csvReader.next());
							if(!callChildScript(customCommand, iterationNo, scriptParameters))
							{
								return false;
							}
							iterationNo++;
						}
					}
					catch (Exception e)
					{
						scriptParameters.put("Error", "Failed to read data file");
						return false;
					}
				}
				else if(param.toLowerCase().startsWith("select "))
				{
					// A select statement
					String databaseDriverJar=scriptParameters.getProperty("DatabaseDriverJar", "mysql-connector-java-5.1.23-bin.jar");
					String databaseDriverClass=scriptParameters.getProperty("DatabaseDriverClass", "com.mysql.jdbc.Driver");
					String databaseConnection=scriptParameters.getProperty("DatabaseConnection", "jdbc:mysql://localhost/sakila?user=root&password=password");
					String table=scriptRunner.executeSql(databaseDriverJar, databaseDriverClass, databaseConnection, param);
					if(table!=null)
					{
						try
						{
							CSVReader csvReader = new CSVReader(new StringReader(table));
							int iterationNo=1;
							while (csvReader.hasNext())
							{
								if (scriptRunner.isStop())
								{
									// Continue with next operation
									return false;
								}
								scriptParameters.putAll(csvReader.next());
								if(!callChildScript(customCommand, iterationNo, scriptParameters))
								{
									return false;
								}
								iterationNo++;
							}
						}
						catch (Exception e)
						{
							scriptParameters.put("Error", "Failed to execute select statement: "+e.toString());
							return false;
						}
					}
				}
				else
				{
					// Repeat a number of times
					int iterations=string2Int(param);
					for (int iterationNo = 0; iterationNo<iterations; iterationNo++)
					{
						if (scriptRunner.isStop())
						{
							// Continue with next operation
							return false;
						}
						if(!callChildScript(customCommand, iterationNo+1, scriptParameters))
						{
							return false;
						}
					}
				}
			}
		}
		return true;
	}

	private boolean callChildScript(CustomCommand customCommand, int iterationNo, Properties scriptParameters)
	{
		int lineNo=scriptRunner.getCurrentLineNo();
		int noScriptLines=customCommand.getNoScriptLines();
		if(!scriptRunner.callChildScript(lineNo-noScriptLines-1, iterationNo, customCommand.getScript(), scriptParameters))
		{
			scriptParameters.putAll(scriptRunner.getParameters());
			return false;
		}
		scriptParameters.putAll(scriptRunner.getParameters());
		return true;
	}

	public String getTooltip()
	{
		return "<html>Perform block while image exist or expression is true<br/>Usage: <br/>"+COMMAND+"</html>";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public String[] getParameters()
	{
		return new String[]{"Error", "Iteration"};
	}

	public void setScriptRunner(ScriptRunner scriptRunner)
	{
		this.scriptRunner=scriptRunner;
	}

	/**
	 * Converts to s to an int
	 * @return An int or 0 if failed
	 */
	private static int string2Int(String s)
	{
		return string2Int(s, 0);
	}

	/**
	 * Converts to s to an int
	 * @return An int or an alternative int if failed
	 */
	private static int string2Int(String s, int otherwise)
	{
		String text = s.trim();
		if(s.length()==0)
		{
			// Blank
			return 0;
		}
		char c = text.charAt(0);
		if (c == '+')
		{
			// Remove + sign
			text=text.substring(1);
		}
		try
		{
			return Integer.parseInt(text);
		}
		catch (Exception e)
		{
			// Couldn't convert to int
			return otherwise;
		}
	}
}
