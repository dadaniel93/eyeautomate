package custom.extended;

import java.util.Properties;

import eyeautomate.HttpServiceCaller;

public class GetRequest
{
	private static final String COMMAND="GetRequest";
	private static final String ICON_FILENAME="icons/call2.png";

	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		if(commandParameters.length<1)
		{
			scriptParameters.put("Error", "Missing parameter in command. Usage: "+COMMAND+" HttpRequest");
			return false;
		}
		try
		{
			String address=commandParameters[0].trim();
			HttpServiceCaller service=new HttpServiceCaller();
			String response=service.executeGetRequest(address);
			if(response==null)
			{
				scriptParameters.put("Error", "Failed to call: "+address);
				return false;
			}
			else
			{
				scriptParameters.put("Response", response);
				return true;
			}
		}
		catch(Exception e)
		{
			scriptParameters.put("Error", "Call failed: "+e.toString());
			return false;
		}
	}

	public String getTooltip()
	{
		return "Calls a web service";
	}

	public String[] getParameters()
	{
		return new String[]{"Error", "Response"};
	}
	
	public String getCommand()
	{
		return "GetRequest <String: Enter HTTP request>";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}
}
