package custom.extended;

import java.util.Properties;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

/**
 * Calculator based on a JavaScript wrapper
 * @author Vladimir Turov, Nordea
 */
public class Calculator
{
	private static final String COMMAND="Calculator";
	private static final String ICON_FILENAME="icons/question2.png";

	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		if(commandParameters.length<1)
		{
			scriptParameters.put("Error", "Missing parameter in command. Usage: Calculator Expression");
			return false;
		}
		try
		{
			StringBuffer textBuffer=new StringBuffer();
			for(String commandParameter:commandParameters)
			{
				if(textBuffer.length()>0)
				{
					textBuffer.append(" ");
				}
				String textParam = commandParameter;
				textBuffer.append(textParam);
			}
			String expression = textBuffer.toString();

			ScriptEngineManager mgr = new ScriptEngineManager();
			ScriptEngine engine = mgr.getEngineByName("JavaScript");
			String result = (engine.eval("("+expression+").toString()")).toString();
			scriptParameters.put("CalculatorResult", result);
			return true;
		}
		catch (Exception e)
		{
			scriptParameters.put("Error", "Failed to evaluate expression");
		}
		return false;
	}

	public String getTooltip()
	{
		return "<html>Calculates an expression<br/>Usage:<br/>"+COMMAND+" Expression</html>";
	}
	
	public String[] getParameters()
	{
		return new String[]{"Error", "CalculatorResult"};
	}
	
	public String getCommand()
	{
		return "Calculator \"<String: Enter expression>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/extendedcommands.html";
	}
}
