package custom.text;

import java.util.Properties;

public class Replace
{
	private static final String COMMAND="Replace";
	private static final String SETS_PARAMETER="Replaced";
	private static final String ICON_FILENAME="icons/analysis2.png";

	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		if(commandParameters.length<3)
		{
			scriptParameters.put("Error", "Missing parameter in command. Usage: "+COMMAND+" Text From To");
			return false;
		}

		String text=commandParameters[0];
		String from=commandParameters[1];
		String to=commandParameters[2];
		String replaced=text.replace(from, to);
		scriptParameters.put(SETS_PARAMETER, replaced);
		return true;
	}

	public String getTooltip()
	{
		return "<html>Replaces each subtext with another<br/>Usage: <br/>"+COMMAND+" Text From To<br/>Sets parameter: "+SETS_PARAMETER+"</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error", SETS_PARAMETER};
	}

	public String getCommand()
	{
		return COMMAND+" \"<String: Enter the original text>\" \"<String: Enter text to replace>\" \"<String: Enter text to insert>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}
}
