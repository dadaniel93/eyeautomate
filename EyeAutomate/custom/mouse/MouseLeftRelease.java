package custom.mouse;

import java.util.Properties;

import eyeautomate.ScriptRunner;

public class MouseLeftRelease
{
	private static final String COMMAND="MouseLeftRelease";
	private static final String ICON_FILENAME="icons/mouse2.png";
	private ScriptRunner scriptRunner=null;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		scriptRunner.mouseLeftRelease();
		return true;
	}

	public String getTooltip()
	{
		return "<html>Left mouse button release<br/>Usage:<br/>"+COMMAND+"</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		return COMMAND;
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public void setScriptRunner(ScriptRunner scriptRunner)
	{
		this.scriptRunner=scriptRunner;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/mousecommands.html";
	}
}
