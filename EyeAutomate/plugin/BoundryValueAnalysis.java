package plugin;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import eyeserver.Action;
import eyeserver.AppState;
import eyeserver.EyeServer;
import eyeserver.Path;
import eyeserver.TypeAction;

public class BoundryValueAnalysis
{
	public List<Action> getSuggestions(AppState stateTree, AppState currentState)
	{
		List<Action> suggestions=new ArrayList<Action>();
		List<String> suggestedValues=new ArrayList<String>();
		List<Action> actions=currentState.getActions();
		for(Action action:actions)
		{
			if(action instanceof TypeAction)
			{
				// Get mutations of action
				Path currentPath=stateTree.getCurrentPath();
				List<Action> pathActions=currentPath.getActions();
				pathActions.add(action);
				List<Action> clonesAndMutations=stateTree.getClonesAndMutations(pathActions);

				// Get values from all clones and mutations
				List<String> allValues=new ArrayList<String>();
				for(Action value:clonesAndMutations)
				{
					TypeAction typeAction=(TypeAction)value;
					allValues.add(typeAction.getText());
				}
				
				if(allValues.size()>0)
				{
					// We have previous values
					if(isIntList(allValues))
					{
						// Integers
						final String[] integerSuggestions={"-1", "0", "1"};
						for(String integerSuggestion:integerSuggestions)
						{
							if(!containsValue(allValues, integerSuggestion) && !containsValue(suggestedValues, integerSuggestion))
							{
								TypeAction suggestion=new TypeAction((TypeAction)action);
								suggestion.setText(integerSuggestion);
								suggestion.setComment(EyeServer.translateCommand("SinceBoundryValue"));
								suggestions.add(suggestion);
								suggestedValues.add(integerSuggestion);
							}
						}
					}
					else if(isEmail(allValues))
					{
						// Emails
						List<String> emailSuggestions=new ArrayList<String>();
						String firstEmail=allValues.get(0);
						int index=firstEmail.indexOf('@');
						if(index>=0)
						{
							String withoutName=firstEmail.substring(index);
							emailSuggestions.add(withoutName);
						}
						int lastIndex=firstEmail.lastIndexOf('.');
						if(lastIndex>=0)
						{
							String withoutDomain=firstEmail.substring(0, lastIndex);
							emailSuggestions.add(withoutDomain);
						}
						for(String emailSuggestion:emailSuggestions)
						{
							if(!containsValue(allValues, emailSuggestion) && !containsValue(suggestedValues, emailSuggestion))
							{
								TypeAction suggestion=new TypeAction((TypeAction)action);
								suggestion.setText(emailSuggestion);
								suggestion.setComment(EyeServer.translateCommand("SinceNegativeTest"));
								suggestions.add(suggestion);
								suggestedValues.add(emailSuggestion);
							}
						}
					}
/*
					else
					{
						// Texts
						final String[] integerSuggestions={"   ", "!.:;()=%&"};
						for(String integerSuggestion:integerSuggestions)
						{
							if(!containsValue(allValues, integerSuggestion) && !containsValue(suggestedValues, integerSuggestion))
							{
								TypeAction suggestion=new TypeAction((TypeAction)action);
								suggestion.setText(integerSuggestion);
								suggestion.setComment(EyeServer.translateCommand("SinceNegativeTest"));
								suggestions.add(suggestion);
								suggestedValues.add(integerSuggestion);
							}
						}
					}
*/
				}
			}
		}
		return suggestions;
	}

	private boolean isIntList(List<String> list)
	{
		for(String item:list)
		{
			try
			{
				Integer.parseInt(item.trim());
			}
			catch(Exception e)
			{
				// Not an int
				return false;
			}
		}
		return true;
	}

	private boolean isEmail(List<String> list)
	{
		for(String item:list)
		{
			if(!isEmail(item))
			{
				return false;
			}
		}
		return true;
	}

	private boolean isEmail(String email)
	{
/*
		String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
		java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
		java.util.regex.Matcher m = p.matcher(email);
*/
		Pattern pattern = Pattern.compile("^.+@.+\\..+$");
		Matcher m = pattern.matcher(email);
		return m.matches();
	}
	
	private boolean containsValue(List<String> list, String value)
	{
		for(String item:list)
		{
			if(item.trim().equals(value.trim()))
			{
				return true;
			}
		}
		return false;
	}
}
