package plugin;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.util.List;
import java.util.Properties;

import javax.imageio.ImageIO;

import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.TextAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import eyeserver.Action;
import eyeserver.AppState;
import eyeserver.DoubleClickAction;
import eyeserver.DragAction;
import eyeserver.DragDropAction;
import eyeserver.DragStartAction;
import eyeserver.EyeServer;
import eyeserver.GoHomeAction;
import eyeserver.Issue;
import eyeserver.LeftClickAction;
import eyeserver.MiddleClickAction;
import eyeserver.MoveAction;
import eyeserver.PasteAction;
import eyeserver.Path;
import eyeserver.RightClickAction;
import eyeserver.RunScriptAction;
import eyeserver.ScrollAction;
import eyeserver.TripleClickAction;
import eyeserver.TypeAction;
import eyeserver.Verification;

public class SessionReports
{
	private static Color circleColor=new Color(0, 0, 255, 160);
	private static Color issueRectangleColor=new Color(255, 0, 0, 160);
	private static Color verificationRectangleColor=new Color(0, 255, 0, 160);

	public String generateReport(AppState stateTree, Properties params)
	{
		String productVersion=params.getProperty("product_version");
		
		String html="<html><b>"+EyeServer.translateCommand("SessionReports")+"</b><br><br>";
		
		List<Path> paths=stateTree.getPaths(productVersion);
		for(Path path:paths)
		{
			String dateStr=DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(path.getCreatedDate());
			dateStr=dateStr.replace(':', '.');
			XWPFDocument doc = new XWPFDocument();
			
			if(!addPath(doc, path))
			{
				return "Failed to generate report";
			}
			
			String filename="reports/"+dateStr+".docx";
			try
			{
				FileOutputStream out = new FileOutputStream(filename);
				doc.write(out);
			}
			catch (Exception e)
			{
				return "Failed to save report";
			}
	    
			String line="<a href=\""+filename+"\">"+filename+"</a><br>";
			html+=line;
			
			try
			{
				doc.close();
			}
			catch (IOException e)
			{
			}
		}
		html+="</html>";

		return html;
	}

	private boolean addPath(XWPFDocument doc, Path path)
	{
		String check=EyeServer.translateCommand("Check");
		List<Action> actions=path.getActions();
		for(Action action:actions)
		{
			String comment=action.getComment();
			if(comment!=null && comment.trim().length()>0)
			{
				addComment(doc, comment);
			}
			if(action instanceof MoveAction)
			{
				if(action instanceof LeftClickAction)
				{
					addCommandAndImage(doc, ((MoveAction)action).getImagePath(), EyeServer.translateCommand("LeftClick"));
				}
				else if(action instanceof DoubleClickAction)
				{
					addCommandAndImage(doc, ((MoveAction)action).getImagePath(), EyeServer.translateCommand("DoubleClick"));
				}
				else if(action instanceof RightClickAction)
				{
					addCommandAndImage(doc, ((MoveAction)action).getImagePath(), EyeServer.translateCommand("RightClick"));
				}
				else if(action instanceof MiddleClickAction)
				{
					addCommandAndImage(doc, ((MoveAction)action).getImagePath(), EyeServer.translateCommand("MiddleClick"));
				}
				else if(action instanceof TripleClickAction)
				{
					addCommandAndImage(doc, ((MoveAction)action).getImagePath(), EyeServer.translateCommand("TripleClick"));
				}
				else if(action instanceof DragAction)
				{
					addCommandAndImage(doc, ((MoveAction)action).getImagePath(), EyeServer.translateCommand("Drag"));
				}
				else if(action instanceof DragStartAction)
				{
					addCommandAndImage(doc, ((MoveAction)action).getImagePath(), EyeServer.translateCommand("DragStart"));
				}
				else if(action instanceof DragDropAction)
				{
					addCommandAndImage(doc, ((MoveAction)action).getImagePath(), EyeServer.translateCommand("DragDrop"));
				}
				else
				{
					addCommandAndImage(doc, ((MoveAction)action).getImagePath(), EyeServer.translateCommand("Move"));
				}
			}
			else if(action instanceof TypeAction)
			{
				addText(doc, EyeServer.translateCommand("Type")+" \""+interpretTypedText(((TypeAction)action).getText())+"\"");
			}
			else if(action instanceof PasteAction)
			{
				addText(doc, EyeServer.translateCommand("Paste")+" \""+interpretTypedText(((PasteAction)action).getText())+"\"");
			}
			else if(action instanceof ScrollAction)
			{
				addText(doc, EyeServer.translateCommand("Scroll")+" "+((ScrollAction)action).getSteps()+" "+EyeServer.translateCommand("steps"));
			}
			else if(action instanceof RunScriptAction)
			{
				addText(doc, EyeServer.translateCommand("Start")+" \""+((RunScriptAction)action).getScriptFilename()+"\"");
			}
			else if(action instanceof GoHomeAction)
			{
				addText(doc, EyeServer.translateCommand("GoHome"));
			}
			
			AppState nextState=action.getNextState();
			if(!nextState.isHome())
			{
				// Add any custom checks or issues
				List<Issue> issues=nextState.getIssues();
				for(Issue issue:issues)
				{
					addRectangleAndImage(doc, issue.getImagePath(), issue.getRectangle(), issue.getText(), issueRectangleColor);
				}
				List<Verification> verifications=nextState.getVerifications();
				for(Verification verification:verifications)
				{
					if(!check.equalsIgnoreCase(verification.getText()))
					{
						// A custom text
						addRectangleAndImage(doc, verification.getImagePath(), verification.getRectangle(), verification.getText(), verificationRectangleColor);
					}
				}
			}
		}
		return true;
	}

	private boolean addCommandAndImage(XWPFDocument doc, String imagePath, String text)
	{
		XWPFParagraph paragraph = doc.createParagraph();
    paragraph.setVerticalAlignment(TextAlignment.CENTER);
    XWPFRun r = paragraph.createRun();
		FileInputStream stream;
		try
		{
			String augmentedImagePath=addCircle(imagePath);
			stream = new FileInputStream(augmentedImagePath);
			r.setText(text+" ");
	    r.addPicture(stream, XWPFDocument.PICTURE_TYPE_PNG, augmentedImagePath, Units.pixelToEMU(200), Units.pixelToEMU(100));
	    r.addBreak();
		}
		catch (Exception e)
		{
			return false;
		}
		return true;
	}

	private boolean addRectangleAndImage(XWPFDocument doc, String imagePath, Rectangle rect, String text, Color color)
	{
		XWPFParagraph paragraph = doc.createParagraph();
    paragraph.setVerticalAlignment(TextAlignment.CENTER);
    XWPFRun r = paragraph.createRun();
		FileInputStream stream;
		try
		{
			String augmentedImagePath=addRectangle(imagePath, rect, color);
			stream = new FileInputStream(augmentedImagePath);
			r.setText(text+" ");
	    r.addPicture(stream, XWPFDocument.PICTURE_TYPE_PNG, augmentedImagePath, Units.pixelToEMU(200), Units.pixelToEMU(100));
	    r.addBreak();
		}
		catch (Exception e)
		{
			return false;
		}
		return true;
	}

	private boolean addText(XWPFDocument doc, String text)
	{
		XWPFParagraph paragraph = doc.createParagraph();
    XWPFRun r = paragraph.createRun();
		try
		{
			r.setText(text);
	    r.addBreak();
		}
		catch (Exception e)
		{
			return false;
		}
		return true;
	}

	private boolean addComment(XWPFDocument doc, String text)
	{
		XWPFParagraph paragraph = doc.createParagraph();
    XWPFRun r = paragraph.createRun();
    r.setItalic(true);
		try
		{
			r.setText(text);
	    r.addBreak();
		}
		catch (Exception e)
		{
			return false;
		}
		return true;
	}

	/**
	 * Converts the typed text into readable format
	 * @param typedText
	 * @return A more readable text
	 */
	private String interpretTypedText(String typedText)
	{
		try
		{
			boolean uppercase=false;
			StringBuffer scriptText=new StringBuffer(); 
			for(int i=0; i<typedText.length(); i++)
			{
				String substring=typedText.substring(i);
				if(substring.startsWith("[SHIFT_PRESS]"))
				{
					int index=substring.indexOf("[SHIFT_RELEASE]");
					if(index>0)
					{
						// Found a shift release - check if all characters in between are alphabetic
						String shiftedChars=substring.substring(13, index);
						if(isAlphabetic(shiftedChars))
						{
							// All characters are alphabetic
							uppercase=true;
							i+=12;
						}
						else
						{
							scriptText.append(typedText.charAt(i));
						}
					}
					else
					{
						scriptText.append(typedText.charAt(i));
					}
				}
				else if(uppercase && substring.startsWith("[SHIFT_RELEASE]"))
				{
					uppercase=false;
					i+=14;
				}
				else
				{
					char c=typedText.charAt(i);
					if(uppercase && Character.isAlphabetic(c))
					{
						scriptText.append(Character.toUpperCase(c));
					}
					else
					{
						scriptText.append(typedText.charAt(i));
					}
				}
			}
			return scriptText.toString();
		}
		catch(Exception e)
		{
			return typedText;
		}
	}

	private boolean isAlphabetic(String s)
	{
		for(int i=0; i<s.length(); i++)
		{
			char c=s.charAt(i);
			if(!Character.isAlphabetic(c))
			{
				return false;
			}
		}
		return true;
	}

	/**
	 * Saves an image as png
	 * @param filepath
	 * @return false if failed
	 */
	private boolean savePngImage(BufferedImage image, String filepath)
	{
		try
		{
			ImageIO.write(image, "png", new File(filepath));
			return true;
		}
		catch (Exception e)
		{
			System.out.println("Failed to save image: " + e.toString());
			return false;
		}
	}

	/**
	 * Loads an image from disc
	 * @param filename Image filename to load
	 * @return Image or null if failed
	 */
	private BufferedImage loadImage(String filename)
	{
		try
		{
			File file = new File(filename);
			return ImageIO.read(file);
		}
		catch (Exception e)
		{
			return null;
		}
	}

	private BufferedImage drawOval(BufferedImage image, int centerX, int centerY, Color color, int radiusX, int radiusY, float stroke)
	{
		Graphics2D g2 = image.createGraphics();
		g2.drawImage(image, 0, 0, (int) image.getWidth(), (int) image.getHeight(), null);
		g2.setColor(color);
//		g2.drawLine(centerX - radiusX, centerY, centerX + radiusX, centerY);
//		g2.drawLine(centerX, centerY - radiusY, centerX, centerY + radiusY);
		g2.setStroke(new BasicStroke(stroke));
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.drawOval(centerX - radiusX, centerY - radiusY, radiusX * 2, radiusY * 2);
		g2.dispose();
		return image;
	}

	private BufferedImage drawRectangle(BufferedImage image, Rectangle rect, Color color, float stroke, int arc)
	{
		Graphics2D g2 = image.createGraphics();
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		g2.drawImage(image, 0, 0, (int) image.getWidth(), (int) image.getHeight(), null);
		g2.setColor(color);
		g2.setStroke(new BasicStroke(stroke));
		int x=image.getWidth()/2-((int)rect.getWidth())/2;
		int y=image.getHeight()/2-((int)rect.getHeight())/2;
		g2.drawRoundRect(x-5, y-5, (int)rect.getWidth()+10, (int)rect.getHeight()+10, arc, arc);
		g2.dispose();
		return image;
	}

	private String addCircle(String filename)
	{
		BufferedImage image=loadImage(filename);
		if(image!=null)
		{
			image=drawOval(image, image.getWidth()/2, image.getHeight()/2, circleColor, 30, 30, 4);
			String augmentedFilename=filename.substring(0, filename.length()-4)+"a"+filename.substring(filename.length()-4);
			if(savePngImage(image, augmentedFilename))
			{
				return augmentedFilename;
			}
		}
		return null;
	}

	private String addRectangle(String filename, Rectangle rect, Color color)
	{
		BufferedImage image=loadImage(filename);
		if(image!=null)
		{
			image=drawRectangle(image, rect, color, 4, 10);
			String augmentedFilename=filename.substring(0, filename.length()-4)+"a"+filename.substring(filename.length()-4);
			if(savePngImage(image, augmentedFilename))
			{
				return augmentedFilename;
			}
		}
		return null;
	}
}
